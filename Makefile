.PHONY: run clean clean-run

dimensions: dimensions.c
	clang -Wall -Wextra -Werror -O2 -std=c99 -pedantic dimensions.c -o dimensions


run: dimensions
	./dimensions

clean:
	rm -f dimensions


format:
	clang-format -i dimensions.c

