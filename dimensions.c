/* https://stackoverflow.com/questions/1022957/getting-terminal-width-in-c */
/* https://www.unitconverters.net/typography/character-x-to-pixel-x.htm */
#include <stdio.h>
#include <sys/ioctl.h>
#include <unistd.h>

int main() {
  struct winsize w;
  ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
  printf("lines %d\n", w.ws_row);
  printf("columns %d\n", w.ws_col);
  printf("pixel height %u\n", w.ws_row * 8);
  printf("pixel width %u\n", w.ws_col * 8);
  return 0; // make sure your main returns int
}
