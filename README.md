# Dimensions 

show the dimensions of the current terminal in lines, columns, and pixels

## Usage: 

```
$ dimensions
lines 53
columns 191
pixel height 424
pixel width 1528

```

## Install

current installation is building from source. This project is built with clang version:

```
$ clang -v
Ubuntu clang version 12.0.0-3ubuntu1~21.04.2
Target: x86_64-pc-linux-gnu
Thread model: posix
InstalledDir: /usr/bin
Found candidate GCC installation: /usr/bin/../lib/gcc/x86_64-linux-gnu/10
Found candidate GCC installation: /usr/bin/../lib/gcc/x86_64-linux-gnu/11
Found candidate GCC installation: /usr/bin/../lib/gcc/x86_64-linux-gnu/9
Found candidate GCC installation: /usr/lib/gcc/x86_64-linux-gnu/10
Found candidate GCC installation: /usr/lib/gcc/x86_64-linux-gnu/11
Found candidate GCC installation: /usr/lib/gcc/x86_64-linux-gnu/9
Selected GCC installation: /usr/bin/../lib/gcc/x86_64-linux-gnu/11
Candidate multilib: .;@m64
Selected multilib: .;@m64

```

and can be compiled by running 

```
make
```

# Development Dependencies

## Ubuntu
```
sudo apt install -y clang clang-format build-essential
```

optional dependencies:
```
sudo apt install -y clangd
```

